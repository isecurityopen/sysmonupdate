# SysmonUpdate

This project is a free update tool for our customers to update Sysmon agents to the latest version. 

Current Sysmon version: 14.16

Check Sysmon docs at https://learn.microsoft.com/en-us/sysinternals/downloads/sysmon

Project Maintained by Gabriel Wagnitz <gabriel.wagnitz@isecurirtyconsulting.com>



## Instructions :
To update the Sysmon version that is running on your server, you first need to remove the existing one and then install the current Sysmon version.

We have provided a script to help you do this process, you can also perform the steps manually. In the Zip file you will find the update script (update-sysmon.ps1) as well as the binaries for version 14.16 - both 32 and 64 - and the sysmonconfig.xml file. Some servers despite being 64bit can only install Sysmon.exe

### Script Requirements:

- Powershell 4.0 or higher
- Windows Server 2012 R2 or higher

If you server has restricted execution policy for scripts (which is advised) you may need to run the script with the following command:


`PowerShell.exe -ExecutionPolicy UnRestricted -File .\update-sysmon.ps1`

More about Sysmon: [Sysmon - Sysinternals | Microsoft Learn](https://learn.microsoft.com/en-us/sysinternals/downloads/sysmon)

Sysmon runs as a service and the default location of the executable is `C:\Windows\Sysmon.exe` or `C:\Windows\Sysmon64.exe`, you can check the current version by running:

`C:\Windows\Sysmon.exe -h`

### Manual Process:
In case you prefer doing the update manually or the script is not running properly, you can perform the following steps:

1. Check what version of sysmon you have:
Check if the sysmon you have installed is Sysmon or Sysmon64, by looking at the Services tab and on “C:\Windows” folder. You can also check the version by running the command shown above


    `C:\Windows\Sysmon.exe -h` or `C:\Windows\Sysmon64.exe -h`

    ![Sysmon version](/img/sysmon_version.png)

    The results shows up as the image above

2. Uninstall existing Sysmon

    `C:\Windows\Sysmon.exe -u force` or  `C:\Windows\Sysmon64.exe -u force`

3. Run on Sysmon64.exe on cmd passing the config file:

    It will ask you to accept the terms


    `.\Sysmon64.exe -i .\sysmonconfig.xml`

    If you have any issues on the install you can try to run Sysmon.exe instead

4. Check if the new version is installed:


    `C:\Windows\Sysmon.exe -h` or `C:\Windows\Sysmon64.exe -h`

    Check the Sysmon service is Present, Running and Automatic on Startup:

    ![Sysmon service](/img/sysmon_service.png)

5. Restart the winlogbeats service:


    `Restart-Service -Name "winlogbeat"`

    You can also restart the winlogbeat using the "Services"

6. Check if the winlogbeats service is running:

    `Get-Service -Name winlogbeat`
